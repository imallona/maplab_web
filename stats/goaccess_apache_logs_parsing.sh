#!/bin/bash
##
## goaccess-based apache logs tiny report
##
## Izaskun Mallona, 12th jan 2017
##
## crontab
## 0 0 * * * bash /imppc/labs/maplab/imallona/src/web/stats/goaccess_apache_logs_parsing.sh

## this contains wanderer, chainy etc
MAP_LOG="gattaca:/data/apache/logs/maplab_access.log"
## this methylation plotter, the trackhub and so on (requires grepping)
OUTER_LOG="gattaca:/data/apache/logs/gattaca_access.log"
HOME=/home/labs/maplab/imallona
WD=$HOME/maplab_logs
OUT="$WD"/visits_stats.html
GOACCESS=/soft/bin/goaccess

mkdir -p $WD
cd $WD

if [ -f "$OUT" ]
then
   mv "$OUT"{,.old}
fi

rsync -a -e 'ssh -i /home/labs/maplab/imallona/.ssh/bitbucket_rsa' "$MAP_LOG" .
rsync -a -e 'ssh -i /home/labs/maplab/imallona/.ssh/bitbucket_rsa' "$OUTER_LOG" .

grep 'chainy\|methylation_plotter\|maplab_trackhub_alpha' gattaca_access.log > gattaca_filtered.log

cat gattaca_filtered.log maplab_access.log > merged.log
"$GOACCESS" -f merged.log \
            --time-format '%H:%M:%S' \
            --date-format '%d/%b/%Y:%H:%M:%S %z' \
            --log-format COMMON \
            --html-report-title="maplab tools usage stats" \
            --html-prefs='{"theme":"bright","perPage":5,"layout":"horizontal","showTables":true}' \
            --output="$OUT" \
            --ignore-crawlers \
            --enable-panel=GEO_LOCATION \
            --enable-panel=VISITORS \
            --ignore-panel=VISIT_TIMES \
            --ignore-panel=REQUESTS \
            --ignore-panel=REQUESTS_STATIC \
            --ignore-panel=NOT_FOUND \
            --ignore-panel=HOSTS \
            --ignore-panel=OS \
            --ignore-panel=BROWSERS \
            --ignore-panel=VIRTUAL_HOSTS \
            --ignore-panel=REFERRERS \
            --ignore-panel=REFERRING_SITES \
            --ignore-panel=KEYPHRASES \
            --ignore-panel=STATUS_CODES \
            --ignore-panel=REMOTE_USER


if [ -s "$OUT" ]
then
    rsync -a -e 'ssh -i /home/labs/maplab/imallona/.ssh/bitbucket_rsa' "$OUT" \
          gattaca:/data/apache/htdocs/groups/maplab/group
fi
